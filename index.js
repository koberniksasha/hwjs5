"use strict";
// Теоретичні питання
// 1.Опишіть своїми словами, що таке метод об’єкту
// 2.Який тип даних може мати значення властивості об’єкта?
// 3.Об’єкт це посилальний тип даних. Що означає це поняття?

// 1. Метод обєкту - це спеціальна дія або функція, яку можна викликати на обєкті для здійснення певної дії.
// 2. Тип даних, який може мати значення властивості обєкта, може бути будь-яким: числовим, рядком, булевим значенням, масивом, ще одним обєктом, або навіть функцією.
// 3. це означає, що коли ми створюємо змінну і призначаємо їй значення обєкту, ця змінна не зберігає сам обєкт, а лише посилання на обєкт.

let newUser = {};

let createNewUser = () => {
  newUser = {
    _firstName: prompt("Введіть ім'я:"),
    _lastName: prompt("Введіть прізвище:"),

    get firstName() {
      return this._firstName;
    },

    get firstName() {
      return this._lastName;
    },

    setFirstName: function (newFirstName) {
      this._firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this._lastName = newLastName;
    },
    getLogin: function () {
      return `${this._firstName[0]}${this._lastName}`.toLowerCase();
    },
  };
  return newUser;
};

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
Object.defineProperty(newUser, "_lastName", {
  writable: false,
});

createNewUser();

console.log(newUser.getLogin());
